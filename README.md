# Запуск
Вы находитесь в папке проекта.
Версия docker: Docker version 19.03.6, build 369ce74a3c
```bash
$ docker-compose up -d --build # Сборка и запуск
$ docker exec -it php bash
$ compose install
$ php artisan make:migration
$ php artisan make:seeder
```
***
Для документов:

List: метод:GET, URL:http://127.0.0.37/api/documents - список документов

Create: метод:POST, URL:http://127.0.0.37/api/documents - создать документ

Show: метод:GET, URL:http://127.0.0.37/api/documents/{id} - просмотр документа

Update: метод:PUT, URL:http://127.0.0.37/api/documents/{id} - изменить документ

Delete: метод:DELETE, URL:http://127.0.0.37/api/documents/{id} - удалить документ
***
Для категорий

List: метод:GET, URL:http://127.0.0.37/api/category - список категорий

Create: метод:POST, URL:http://127.0.0.37/api/category - создать категорию

Show: метод:GET, URL:http://127.0.0.37/api/category/{id} - просмотр категории со всеми документами текущей категории

Update: метод:PUT, URL:http://127.0.0.37/api/category/{id} - изменить описание категории

Delete: метод:DELETE, URL:http://127.0.0.37/api/category/{id} - удалить категорию вместе с документами этой категории
