<?php

namespace App\Http\Controllers\Api;

use App\Document;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class DocumentController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $documents = Document::all();
        return $this->sendResponse($documents->toArray(), 'Документы успешно получены');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, Document::VALIDATION_RULES, Document::MESSAGES_VALIDATION_ERRORS);

        if($validator->fails()){
            return $this->sendError('Ошибка валидации.', $validator->errors());
        }

        $input['created_at'] = Carbon::now();
        $input['status'] = Document::STATUS_DRAFT;

        $document = Document::create($input);
        return $this->sendResponse($document->toArray(), 'Документ создан');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $document = Document::find($id);
        if (is_null($document)) {
            return $this->sendError('Документ не найден.');
        }
        return $this->sendResponse($document->toArray(), 'Документ найден');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Document  $document
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Document $document)
    {
        $input = $request->all();
        $validator = Validator::make($input, Document::VALIDATION_RULES, Document::MESSAGES_VALIDATION_ERRORS);
        if($validator->fails()){
            return $this->sendError('Ошибка валидации.', $validator->errors());
        }
        $document->code = $input['code'];
        $document->title = $input['title'];
        $document->short_description = $input['short_description'];
        $document->description = $input['description'];
        $document->status = $input['status'];
        $document->save();
        return $this->sendResponse($document->toArray(), 'Изменения документа сохранены');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Document $document
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Document $document)
    {
        $document->delete();
        return $this->sendResponse($document->toArray(), 'Документ сожжен');
    }
}
