<?php

namespace App\Http\Controllers\Api;

use App\Category;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CategoryController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $categories = Category::all();
        return $this->sendResponse($categories->toArray(), 'Категории документов');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, Category::VALIDATION_RULES, Category::MESSAGES_VALIDATION_ERRORS);

        if($validator->fails()){
            return $this->sendError('Ошибка валидации.', $validator->errors());
        }

        $input['created_at'] = Carbon::now();

        $category = Category::create($input);
        return $this->sendResponse($category->toArray(), 'Категория создана');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $category = Category::find($id);
        if (is_null($category)) {
            return $this->sendError('Категория не найдена.');
        }
        return $this->sendResponse($category->categoryToArray(), 'Категория найдена');
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Category  $category
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Category $category)
    {
        $input = $request->all();
        $validator = Validator::make($input, Category::UPDATE_VALIDATION_RULES, Category::MESSAGES_VALIDATION_ERRORS);
        if($validator->fails()){
            return $this->sendError('Ошибка валидации.', $validator->errors());
        }
        $category->description = $input['description'];
        $category->save();
        return $this->sendResponse($category->toArray(), 'Изменения категории сохранены');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  Category $category
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Category $category)
    {
        $category->delete();
        return $this->sendResponse($category->toArray(), 'Категория истреблена');
    }
}
