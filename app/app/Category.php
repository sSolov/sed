<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'category';

    public $timestamps = false;

    public const VALIDATION_RULES = [
        'title' => 'required|string',
        'description' => 'required|string',
    ];

    public const UPDATE_VALIDATION_RULES = [
        'description' => 'required|string',
    ];

    public const MESSAGES_VALIDATION_ERRORS = [
        'title.required' => 'Название категории обязательно!',
        'description.required' => 'Для чего эта катгория документов',
    ];

    protected $fillable = [
        'id', 'title', 'description', 'created_at',
    ];

    /**
     * Получить документы текущей категории.
     */
    public function documents()
    {
        return $this->hasMany('App\Document');
    }

    public function delete()
    {
        $this->documents()->delete();
        return parent::delete();
    }

    public function categoryToArray()
    {
        $category = parent::toArray();
        $category['documents'] = $this->documents;
        return $category;
    }
}
