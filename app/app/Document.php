<?php

declare(strict_types=1);

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $table = 'documents';

    public const STATUS_DRAFT = 'draft';
    public const STATUS_SINGNING = 'signing';
    public const STATUS_ACTIVE = 'active';
    public const STATUS_ARCHIVE = 'archival';

    public const VALIDATION_RULES = [
        'code' => 'required|string',
        'title' => 'required|string',
        'short_description' => 'required|string',
        'description' => 'required|string',
        'category_id' => 'required',
        'status' => 'in:' . Document::STATUS_DRAFT . ',' . Document::STATUS_SINGNING . ',' . Document::STATUS_ACTIVE . ',' . Document::STATUS_ARCHIVE
    ];

    public const MESSAGES_VALIDATION_ERRORS = [
        'code.required' => 'Код обязателен для заполнения!',
        'title.required' => 'Название документа обязательно!',
        'short_description.required' => 'Краткое описание обязательно для заполнения!',
        'description.required' => 'В чем состав документа?',
        'category_id.required' => 'Категория документа обязательна',
        'status.in' => 'Статус: '. Document::STATUS_DRAFT . ' или ' . Document::STATUS_SINGNING . ' или ' . Document::STATUS_ACTIVE . ' или ' . Document::STATUS_ARCHIVE,
    ];

    public $timestamps = false;

    protected $fillable = [
        'id', 'code', 'title', 'short_description', 'description', 'status', 'category_id', 'created_at',
    ];

    /**
     * Получить категорию данного документа.
     */
    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function toArray()
    {
        $document = parent::toArray();
        if(isset($document['category_id'])) {
            unset($document['category_id']);
        }
        $document['category'] = $this->category;
        return $document;
    }
}
