<?php

declare(strict_types=1);

use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    private const STATUS = ['draft', 'signing', 'active', 'archival'];

    private const CATEGORIES = [
        'Соглашение',
        'Договор',
        'Заявление',
        'Служебная записка',
        'Приказ',
        'Отчет',
        'Грамота',
        'Счет-фактура',
        'Взыскание',
        'Иной документ',
    ];

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        for($i=0; $i<10; $i++) {
            DB::table('category')->insert([
                'title' => Arr::random(self::CATEGORIES),
                'description' => $this->generateText(15, 12),
                'created_at' => $this->generateDate(),
            ]);
        }

        for($i=0; $i<20; $i++) {
            DB::table('documents')->insert([
                'code' => Str::random(8),
                'title' => Str::random(10).' '.Str::random(3).' '.Str::random(15),
                'short_description' => $this->generateText(15, 12),
                'description' => $this->generateText(200, 15),
                'status' => Arr::random(self::STATUS),
                'category_id' => mt_rand(0, 10),
                'created_at' => $this->generateDate(),
            ]);
        }
    }

    private function generateText(int $countWords, int $maxLengthOneWord): string
    {
        $text = Str::random($maxLengthOneWord);

        for($i=0; $i<$countWords; $i++) {
            $randomLength = mt_rand(0, $maxLengthOneWord);

            $text .= ' '.Str::random($randomLength);
        }

        return $text;
    }

    private function generateDate()
    {
        return Carbon::create(mt_rand(1970, 2020), mt_rand(0, 12), mt_rand(0, 29), mt_rand(0, 60),mt_rand(0, 60), mt_rand(0, 60), null);
    }
}
